#!/bin/bash

#On verifie que l'user est root avec la commande whoami
if [ "$(whoami)" = "root" ]; then
	echo"[...] update database [...] "
	apt-get upgrade
	echo"[...] update system [...] "
	apt-get update
else
	echo "[/!\] Vous devez être super-utilisateur [/!\]"

fi

