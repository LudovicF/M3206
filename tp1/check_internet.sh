#!/bin/bash

if ping -c 1 8.8.8.8 >/dev/null; then
	echo "[...] Cheking internet connection [...]"
	echo "[...] Internet access OK	[...]"
else	
	echo "[...] Cheking internet connection [...]"
	echo "[/!\] Not connected to Internet	[/!\]"
	echo "[/!\] Please check configuration	[/!\]"
fi
